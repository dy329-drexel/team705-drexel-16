package com.example.deepak.foodtracker;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Deepak on 5/30/2016.
 */
public class foodTruck implements Serializable{

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    String Name;
    String Address;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public ArrayList<menuitem> getFooditem() {
        return fooditem;
    }

    public void setFooditem(ArrayList<menuitem> fooditem) {
        this.fooditem = fooditem;
    }

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }

    public double getDis() {
        return dis;
    }

    public void setDis(double dis) {
        this.dis = dis;
    }

    String timing;
    String type;
    ArrayList<menuitem> fooditem;

    double lat;
    double longi;
    double dis;

    public foodTruck(String a,String address,String ol,double b,double c,ArrayList<menuitem> item){
        Name=a;
        lat=b;
        longi=c;
       Address=address;
        timing=ol;
        type="not defined";
        fooditem=item;


    }
    public foodTruck(String a,String address,String ol,double b,double c){
        Name=a;
        lat=b;
        longi=c;
        Address=address;
        timing=ol;
        type="not defined";
        fooditem=null;


    }



    public double getlat(){
        return lat;
    }
    public double getlongi(){
        return longi;
    }




}
