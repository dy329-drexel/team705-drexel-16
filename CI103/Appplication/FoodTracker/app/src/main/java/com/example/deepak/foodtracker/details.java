package com.example.deepak.foodtracker;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class details extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView label1 = (TextView) findViewById(R.id.nametext);
        TextView label2 = (TextView) findViewById(R.id.addresstext);
        TextView label3=(TextView)findViewById(R.id.textView10);

        Button b=(Button)findViewById(R.id.getdir);
        Button back=(Button)findViewById(R.id.backb);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent otherIntent = new Intent(details.this, searchscreen.class);



                startActivity(otherIntent);
            }
        });

Intent i=getIntent();
        final foodTruck abc = (foodTruck)i.getSerializableExtra("foodTruck");
        label1.setText(abc.getName());
        label2.setText(abc.getAddress());
        label3.setText(abc.getTiming());
        Log.d("objected adadf", "" + abc.getTiming());


        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showmap(abc);
            }
        });



        ListView lv = (ListView) findViewById(R.id.listView2);

        ArrayList<menuitem> items =abc.getFooditem();

        menuAdapter adapter = new menuAdapter(details.this, items);

        lv.setAdapter(adapter);
    }
    public  void showmap(foodTruck xyz){



        Intent otherIntent = new Intent(details.this, MapsActivitydir.class);
        otherIntent.putExtra("foodTruck", xyz);


        startActivity(otherIntent);

    }
}
