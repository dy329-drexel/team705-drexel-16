package com.example.deepak.foodtracker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Deepak on 6/3/2016.
 */
public class menuAdapter extends ArrayAdapter<menuitem> {


    public menuAdapter(Context context, ArrayList<menuitem> details) {
        super(context, 0, details);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position

         menuitem det1 = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_menu, parent, false);
        }
        // Lookup view for data population
        TextView name = (TextView) convertView.findViewById(R.id.nameView);
        TextView price = (TextView) convertView.findViewById(R.id.priceView);










        //   Populate the data into the template view using the data object
        name.setText(det1.name);
        price.setText("$"+det1.price);


        // abc.setImageBitmap(det1.bmp);
        // Return the completed view to render on screen
        return convertView;
    }



}
