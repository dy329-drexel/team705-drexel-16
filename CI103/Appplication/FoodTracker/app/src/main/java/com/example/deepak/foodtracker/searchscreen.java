package com.example.deepak.foodtracker;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;

public class searchscreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchscreen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final EditText searchbox = (EditText) findViewById(R.id.editText);
        Button go=(Button)findViewById(R.id.find);
        Button findloc=(Button)findViewById(R.id.button3);

        findloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<foodTruck> xyz = new ArrayList<foodTruck>();
                Location l = getLoc();
                double lala1 = l.getLatitude();
                double longi1 = l.getLongitude();
                final Findtrucks task1 = new Findtrucks(xyz, lala1, longi1);


                task1.execute();

                Button b1 = (Button) findViewById(R.id.button2);
                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ArrayList<foodTruck> abc = task1.getalldata();
                        showmap(abc);
                    }
                });
            }
        });


                assert go != null;
                go.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        String search = searchbox.getText().toString();

                        ArrayList<foodTruck> items = new ArrayList<foodTruck>();

                        foodAdapter adapter = new foodAdapter(searchscreen.this, items);
                        final findtruck_name Task1 = new findtruck_name(items, adapter, searchscreen.this, search);

                        ListView list = (ListView) findViewById(R.id.listView);
                        list.setAdapter(adapter);


                        Task1.execute();


                        ListView abc = (ListView) findViewById(R.id.listView);
                        abc.setOnItemClickListener(new AdapterView.OnItemClickListener()

                        {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position,
                                                    long id) {


                                foodTruck itemselect = Task1.getselecteditem(position);
                                startIntent(itemselect);
                                //http://stackoverflow.com/questions/2736389/how-to-pass-an-object-from-one-activity-to-another-on-android


                            }


                        });
                    }
                    });

                }






            public void startIntent(foodTruck itemselect) {


                Intent othernewIntent = new Intent(searchscreen.this, details.class);

                //http://stackoverflow.com/questions/2736389/how-to-pass-an-object-from
                Log.d("objected adadf", "" + itemselect.getAddress());
                othernewIntent.putExtra("foodTruck", itemselect);
                startActivity(othernewIntent);
            }

            public Location getLoc() {

                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


                    double lala1 = 39.956727;
                    double longi1 = -75.189523;

                    Location loc = new Location("");
                    loc.setLatitude(lala1);
                    loc.setLongitude(longi1);
                    return loc;

                }
                Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Log.d("location from gps", "" + "" + locationGPS.getLongitude());
                return locationGPS;
            }



   public  void showmap(ArrayList<foodTruck> xyz){
       Log.d("size of array", "hehehe" + xyz.size());
       arraylistoftrucks arr=new arraylistoftrucks();
       arr.setAbc(xyz);
       Intent otherIntent = new Intent(searchscreen.this, MapsActivity.class);
       otherIntent.putExtra("arr", arr);


       startActivity(otherIntent);

   }

}