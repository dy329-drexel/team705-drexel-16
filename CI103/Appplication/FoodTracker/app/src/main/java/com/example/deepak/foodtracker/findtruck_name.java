package com.example.deepak.foodtracker;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectGet;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectSearch;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import java.util.ArrayList;

/**
 * Created by Deepak on 5/30/2016.
 */
public class findtruck_name extends AsyncTask<Void,Void,Void>{

    ArrayList<foodTruck> data1;
    foodAdapter adapter;
    Context context;
    String keyword;
    ArrayList<menuitem> itemlist;


    public foodTruck getselecteditem(int pos){
        return data1.get(pos);

    }




    public findtruck_name(ArrayList<foodTruck> data, foodAdapter adapter, Context context, String keyword) {
        this.data1 = data;
        this.adapter = adapter;
        this.context = context;
        this.keyword = keyword;
        itemlist=new ArrayList<menuitem>();
    }


    @Override
    protected Void doInBackground(Void... params) {


        TembooSession session = null;
        try {
            session = new TembooSession("phuc", "myFirstApp", "SbH5AuKMLucITMPTxrINATU1uqw4coup");
        } catch (TembooException e) {
            e.printStackTrace();
        }

        ObjectSearch objectSearchChoreo = new ObjectSearch(session);

// Get an InputSet object for the choreo
        ObjectSearch.ObjectSearchInputSet objectSearchInputs = objectSearchChoreo.newInputSet();

// Set inputs
        objectSearchInputs.set_APIKey("ec572ef0e21d4bdfb829ec82bd3ab239");
        objectSearchInputs.set_Query("[name=\"" + keyword + "\"]");
        objectSearchInputs.set_ApplicationIdentifier("7ff5d49b2e0bf34baa9a9e0e8dad7a82");

// Execute Choreo
        try {
            ObjectSearch.ObjectSearchResultSet objectSearchResults = objectSearchChoreo.execute(objectSearchInputs);



            JsonParser gson1 = new JsonParser();
            JsonElement root1 = gson1.parse(objectSearchResults.get_Response());
            JsonObject rootobj1 = root1.getAsJsonObject();
            JsonObject rootobj2 = rootobj1.get("success").getAsJsonObject();
           JsonObject rootobj3=rootobj2.get(keyword).getAsJsonObject();

            String name = rootobj3.get("name").getAsString();
            String address =rootobj3.get("Address").getAsString();
            String timing = rootobj3.get("Hours").getAsString();

            double lati = rootobj3.get("lat").getAsDouble();
            double longi =rootobj3.get("long").getAsDouble();
            JsonArray ar = rootobj3.get("Menu").getAsJsonArray();

            for (int i = 0; i < ar.size(); i++) {
               JsonObject data = ar.get(i).getAsJsonObject();
             String iname=data.get("name").getAsString();
                Log.d("longitudes", "" +iname);
             float price=data.get("Price").getAsFloat();
            menuitem newitem=new menuitem(iname,price);
                itemlist.add(newitem);
            }

                foodTruck abc=new foodTruck(name,address,timing,lati,longi,itemlist);
                data1.add(abc);





        } catch (Exception e) {
            menuitem i=new menuitem();
            foodTruck abc=new foodTruck("no match found","-","-",0,0);
            data1.add(abc);
            e.printStackTrace();
        }


        return null;
    }
    @Override
    protected void onPostExecute(Void param)
    {
        adapter.notifyDataSetChanged();
    }
}
