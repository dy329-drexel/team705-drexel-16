package com.example.deepak.foodtracker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Deepak on 5/30/2016.
 */

public class foodAdapter extends ArrayAdapter<foodTruck>{


    public foodAdapter(Context context, ArrayList<foodTruck> details) {
        super(context, 0, details);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position

        foodTruck det1 = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_truck, parent, false);
        }
        // Lookup view for data population
        TextView name = (TextView) convertView.findViewById(R.id.nameText);
        TextView address = (TextView) convertView.findViewById(R.id.addressText);
        TextView timing = (TextView) convertView.findViewById(R.id.timeText);









        //   Populate the data into the template view using the data object
        name.setText(det1.Name);
        address.setText(det1.Address);
        timing.setText(det1.timing);

        // abc.setImageBitmap(det1.bmp);
        // Return the completed view to render on screen
        return convertView;
    }



}
