package com.example.deepak.foodtracker;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogin;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.


        Button signin_button=(Button)findViewById(R.id.sign_in_button);
        signin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText email = (EditText) findViewById(R.id.email);

                final EditText pw1 = (EditText) findViewById(R.id.password);
                final String uid = email.getText().toString();
                final String pass = pw1.getText().toString();
                Login auth = new Login();
                auth.execute(uid, pass);



            }
        });




    }
    protected void startdisplay(String a){
        String sess=a;

        if(sess=="") {
            Toast.makeText(LoginActivity.this, "Login failed !! Try Again", Toast.LENGTH_SHORT).show();
        }
        else {
            Log.d("stoken", sess);
            Intent otherIntent = new Intent(LoginActivity.this, adminActivity.class);
            otherIntent.putExtra("session_token", sess);
            startActivity(otherIntent);
        }
    }


    public class Login extends AsyncTask<String,Void,String> {


        @Override
        protected String doInBackground(String... params) {
            String email=params[0];
            String pass=params[1];
            String abc="";
            TembooSession session = null;
            try {
                session = new TembooSession("phuc", "myFirstApp", "SbH5AuKMLucITMPTxrINATU1uqw4coup");
            } catch (TembooException e1) {
                e1.printStackTrace();
            }

            AccountLogin accountLoginChoreo = new AccountLogin(session);

// Get an InputSet object for the choreo
            AccountLogin.AccountLoginInputSet accountLoginInputs = accountLoginChoreo.newInputSet();

// Set inputs
            accountLoginInputs.set_APIKey("ec572ef0e21d4bdfb829ec82bd3ab239");
            accountLoginInputs.set_Username(email);
            accountLoginInputs.set_ApplicationIdentifier("7ff5d49b2e0bf34baa9a9e0e8dad7a82");
            accountLoginInputs.set_Password(pass);

            AccountLogin.AccountLoginResultSet accountLoginResults = null;
            try {
                accountLoginResults = accountLoginChoreo.execute(accountLoginInputs);


                JsonParser jp = new JsonParser();
                JsonElement root = jp.parse(accountLoginResults.get_Response());
                JsonObject rootobj=root.getAsJsonObject();

                abc=rootobj.get("session_token").getAsString();
            } catch (TembooException e1) {
                e1.printStackTrace();
            }
            //    sessiont= accountLoginResults.getResultString("session_token");






            return abc;
        }
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            startdisplay(result);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

