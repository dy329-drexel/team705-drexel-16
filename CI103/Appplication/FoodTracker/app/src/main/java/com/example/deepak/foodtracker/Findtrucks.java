package com.example.deepak.foodtracker;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectGet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Deepak on 6/3/2016.
 */
public class Findtrucks extends AsyncTask<Void,Void,Void> {


    ArrayList<foodTruck> data1;

        double plati;
        double plong;

        public Findtrucks(ArrayList<foodTruck> data2,double plati1,double plong1) {
            data1=data2;

            plati=plati1;
            plong=plong1;

        }
        public foodTruck getselecteditem(int pos){
            return data1.get(pos);
        }
        public ArrayList<foodTruck> getalldata(){
            return data1;
        }
        @Override
        protected Void doInBackground(Void... params) {


            TembooSession session = null;
            try {
                session = new TembooSession("phuc", "myFirstApp", "SbH5AuKMLucITMPTxrINATU1uqw4coup");
            } catch (TembooException e1) {
                e1.printStackTrace();
            }

            ObjectGet objectGetChoreo = new ObjectGet(session);

// Get an InputSet object for the choreo
            ObjectGet.ObjectGetInputSet objectGetInputs = objectGetChoreo.newInputSet();

// Set inputs
            objectGetInputs.set_APIKey("ec572ef0e21d4bdfb829ec82bd3ab239");
            objectGetInputs.set_Skip("6");
            objectGetInputs.set_ApplicationIdentifier("7ff5d49b2e0bf34baa9a9e0e8dad7a82");

// Execute Choreo
            ObjectGet.ObjectGetResultSet objectGetResults = null;
            try {
                objectGetResults = objectGetChoreo.execute(objectGetInputs);
            } catch (TembooException e1) {
                e1.printStackTrace();
            }


            JsonParser gson1 = new JsonParser();
                JsonElement root1 = gson1.parse(objectGetResults.get_Response());
                JsonObject rootobj1 = root1.getAsJsonObject();
                JsonObject rootobj2 = rootobj1.get("success").getAsJsonObject();

                JsonArray ar = rootobj2.get("trucks").getAsJsonArray();

                for (int i = 0; i < ar.size(); i++) {
                    JsonObject data = ar.get(i).getAsJsonObject();
                    String name = data.get("Name").getAsString();
                    String address = data.get("Address").getAsString();
                    String timing=data.get("Hours").getAsString();
                    double lati = data.get("lat").getAsDouble();
                    double longi = data.get("long").getAsDouble();
                    //  Log.d("longitudes", "" + plati);
                    double ct = Math.PI / 180;


                    double theta = plong - longi;
                    double dist = Math.sin(ct * plati) * Math.sin(ct * lati) + Math.cos(ct * plati) * Math.cos(ct * lati) * Math.cos(ct * theta);
                    dist = Math.acos(dist);
                    dist = dist * (180 / Math.PI);
                    dist = dist * 60 * 1.1515;
                    double distanceinkm = dist * 1.609344;
                    distanceinkm = Math.floor(distanceinkm * 100) / 100;
                    if ( (distanceinkm <= 1)) {

                        Log.d("name", "" + name);
                            foodTruck abc;
                            abc = new foodTruck(name,address,timing, lati, longi);
                            data1.add(abc);



                    //      person abc;
                    //  abc = new person(email, lati, longi, distanceinkm);
                    //  data1.add(abc);

                }


            }



            return null;
        }



    }
















